﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using TheFishSurvival.Input;

namespace TheFishSurvival.Controls
{
    public class Button
    {
        protected Texture2D background;

        public Button()
        {
            InputService.Instance.DisplayPressed += Instance_DisplayPressed;
        }

        public event EventHandler ButtonPressed;

        public bool IsPressed { get; private set; } = false;

        public Rectangle Position { get; set; }

        public void Init(Texture2D background)
        {
            this.background = background;
        }

        public void Draw(SpriteBatch spriteBatch, Color color)
        {
            spriteBatch.Draw(background, Position, color);
        }

        protected virtual void Instance_DisplayPressed(object sender, EventArgs<Point> e)
        {
            if (Position.Contains(e.Data))
            {
                ButtonPressed?.Invoke(this, EventArgs.Empty);
            }
        }
    }
}