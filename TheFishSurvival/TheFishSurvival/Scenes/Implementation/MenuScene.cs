﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using TheFishSurvival.Scenes.Interfaces;
using TheFishSurvival.Controls;
using TheFishSurvival.Particles;

namespace TheFishSurvival.Scenes.Implementation
{
    public class MenuScene : IScene
    {
        private Texture2D background;
        private MenuButton exitButton;
        private MenuButton startButton;
        private SpriteFont menuFont;
        private Bubbles bubbles;

        private const int buttonOffset = 100;

        public MenuScene(SceneTypes type)
        {
            SceneType = type;
            bubbles = new Bubbles();
        }

        public event EventHandler ExitButtonPressed;
        public event EventHandler StartGamePressed;

        #region Properties

        public SceneTypes SceneType { get; private set; }

        #endregion

        #region Public

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, new Rectangle(new Point(0, 0), new Point(HardwareInformation.ScreenWidth, HardwareInformation.ScreenHeight)), Color.White);
            bubbles.Draw(spriteBatch);
            exitButton.Draw(spriteBatch, menuFont, Color.Red);
            startButton.Draw(spriteBatch, menuFont, Color.Red);
        }

        public void Initialization(ContentManager content)
        {
            background = content.Load<Texture2D>("menubackground");
            menuFont = content.Load<SpriteFont>("menufont");

            var buttonTexture = content.Load<Texture2D>("buttonbackground");
            int exitButtonOffset = HardwareInformation.ScreenHeight - (HardwareInformation.ScreenHeight / 3);
            int startButtonOffset = exitButtonOffset - 200;

            bubbles.Init(content);

            exitButton = new MenuButton();
            exitButton.Init(buttonTexture, "Exit");
            exitButton.Position = new Rectangle(new Point(buttonOffset, exitButtonOffset), new Point(320, 160));
            exitButton.ButtonPressed += ExitButton_ButtonPressed;

            startButton = new MenuButton();
            startButton.Init(buttonTexture, "Start");
            startButton.Position = new Rectangle(new Point(buttonOffset, startButtonOffset), new Point(320, 160));
            startButton.ButtonPressed += StartButton_ButtonPressed;
        }
        
        public void Update(GameTime gameTime)
        {
            bubbles.Update(gameTime);
        }
        
        #endregion

        private void ExitButton_ButtonPressed(object sender, EventArgs e)
        {
            ExitButtonPressed?.Invoke(this, EventArgs.Empty);
        }

        private void StartButton_ButtonPressed(object sender, EventArgs e)
        {
            StartGamePressed?.Invoke(this, EventArgs.Empty);
        }
    }
}