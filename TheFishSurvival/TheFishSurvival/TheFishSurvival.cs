using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using System.Collections.Generic;

using TheFishSurvival.Scenes.Interfaces;
using TheFishSurvival.Scenes.Implementation;

namespace TheFishSurvival
{
    public class TheFishSurvival : Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private List<IScene> scenes;
        private IScene activeScene;

        public TheFishSurvival()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.IsFullScreen = true;
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 480;
            graphics.SupportedOrientations = DisplayOrientation.LandscapeLeft | DisplayOrientation.LandscapeRight;

            activeScene = null;
        }

        private IScene ActiveScene
        {
            get => activeScene;
            set
            {
                if (activeScene != value)
                {
                    if (activeScene != null)
                    {
                        //unregister events
                        switch(activeScene.SceneType)
                        {
                            case SceneTypes.MenuScene:
                                var menu = activeScene as MenuScene;
                                menu.ExitButtonPressed -= Menu_ExitButtonPressed;
                                menu.StartGamePressed -= Menu_StartGamePressed;
                                break;
                            case SceneTypes.GameScene:
                                //TODO
                                break;
                        }
                    }

                    if (value != null)
                    {
                        activeScene = value;
                        //register events
                        switch (activeScene.SceneType)
                        {
                            case SceneTypes.MenuScene:
                                var menu = activeScene as MenuScene;
                                menu.ExitButtonPressed += Menu_ExitButtonPressed;
                                menu.StartGamePressed += Menu_StartGamePressed;
                                break;
                            case SceneTypes.GameScene:
                                //TODO
                                break;
                        }
                    }
                }
            }
        }

        protected override void Initialize()
        {
            base.Initialize();

            HardwareInformation.ScreenWidth = graphics.PreferredBackBufferWidth;
            HardwareInformation.ScreenHeight = graphics.PreferredBackBufferHeight;

            scenes = new List<IScene>();

            InitializationScenes();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void UnloadContent()
        {
            base.UnloadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                TurnOffGame();
            }

            Input.InputService.Instance.DetectInput(gameTime);

            activeScene.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            activeScene.Draw(spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        private void InitializationScenes()
        {
            var menu = new MenuScene(SceneTypes.MenuScene);
            menu.Initialization(Content);
            
            scenes.Add(menu);

            var gameScene = new GameScene(SceneTypes.GameScene);
            gameScene.Initialization(Content);

            scenes.Add(gameScene);

            ActiveScene = scenes[0];
        }

        private void TurnOffGame()
        {
            UnloadContent();
            Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
        }

        private void Menu_StartGamePressed(object sender, System.EventArgs e)
        {
            ActiveScene = scenes[1];
            //init game
        }

        private void Menu_ExitButtonPressed(object sender, System.EventArgs e)
        {
            TurnOffGame();
        }
    }
}
