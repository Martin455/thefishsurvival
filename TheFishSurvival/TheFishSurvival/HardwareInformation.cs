﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace TheFishSurvival
{
    public static class HardwareInformation
    {
        public static int ScreenWidth  { get; set; }
        public static int ScreenHeight { get; set; }
    }
}