﻿namespace TheFishSurvival.Scenes.Interfaces
{
    public enum SceneTypes
    {
        MenuScene,
        GameScene
    }
}