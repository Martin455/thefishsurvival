﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace TheFishSurvival.Scenes.Interfaces
{
    public interface IScene
    {
        SceneTypes SceneType { get; }

        void Initialization(ContentManager content);
        void Update(GameTime gameTime);
        void Draw(SpriteBatch spriteBatch);
    }
}