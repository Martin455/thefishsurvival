﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace TheFishSurvival.Particles
{
    public class Bubbles
    {
        private struct Bubble
        {
            public Vector2 Position;
            public Vector2 Direction;
            public bool IsAlive;
            public float Scale;
            public float Speed;
        }

        private const int numberOfBubbles = 16;
        private Bubble[] bubbles;
        private Texture2D alphaTexture;
        private Random random;

        public Bubbles()
        {
            bubbles = new Bubble[numberOfBubbles];
            random = new Random(DateTime.Now.Millisecond);
        }

        public void Init(ContentManager content)
        {
            alphaTexture = content.Load<Texture2D>("bubble");

            for(int i = 0; i < numberOfBubbles; ++i)
            {
                bubbles[i].IsAlive = false;
            }
        }

        public void Update(GameTime gameTime)
        {
            for (int i = 0; i < numberOfBubbles; ++i)
            {
                if (!bubbles[i].IsAlive)
                {
                    InitBubble(ref bubbles[i]);
                }
                else
                {
                    UpdateBubble(ref bubbles[i]);
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < numberOfBubbles; ++i)
            {
                if (bubbles[i].IsAlive)
                {
                    spriteBatch.Draw(alphaTexture, bubbles[i].Position, null, Color.White, 0.0f, new Vector2(), bubbles[i].Scale, SpriteEffects.None, 0);
                }
            }
        }

        private void InitBubble(ref Bubble bubble)
        {
            int randNumber = random.Next(0, 1024);
            if (randNumber % 7 == 0) //Magic number for allow creating bubble
            {
                bubble.IsAlive = true;

                //x axis
                int x = random.Next(0, HardwareInformation.ScreenWidth);
                //y axis
                int y = HardwareInformation.ScreenHeight + 70;

                bubble.Position = new Vector2(x, y);
                bubble.Direction = new Vector2(0, -1);

                bubble.Scale = (float)random.NextDouble() * 3.5f;
                bubble.Speed = 0.8f + (float)random.NextDouble() * 8.0f;
            }
        }

        private void UpdateBubble(ref Bubble bubble)
        {
            bubble.Position.X += bubble.Direction.X * bubble.Speed;
            bubble.Position.Y += bubble.Direction.Y * bubble.Speed;

            if (bubble.Position.Y < 0)
            {
                bubble.IsAlive = false;
            }
        }
    }
}