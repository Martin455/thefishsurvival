﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TheFishSurvival.Controls
{
    public class MenuButton: Button
    {
        private string text;

        public MenuButton(): base()
        {

        }

        public void Init(Texture2D background, string text)
        {
            base.Init(background);
            this.text = text;
        }

        public void Draw(SpriteBatch spriteBatch, SpriteFont font, Color color)
        {
            base.Draw(spriteBatch, color);

            var stringSize = font.MeasureString(text);

            float halfStringSizeX = stringSize.X / (float)2.0;
            float halfStringSizeY = stringSize.Y / (float)2.0;
            
            spriteBatch.DrawString(font, text, new Vector2(Position.Center.X - halfStringSizeX, Position.Center.Y - halfStringSizeY), Color.Black);
        }
    }
}