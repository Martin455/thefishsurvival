﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;


namespace TheFishSurvival.Entities
{
    public abstract class Entity
    {
        protected Texture2D texture;
        protected float scale;

        public Point Position { get; private set; }

        public bool Intersect(Entity entity)
        {
            return false;
        }

        public abstract void Move(Point point);
        public abstract void Move(int x, int y);

        public abstract void Draw(SpriteBatch spriteBatch);
        public abstract void Update(GameTime gameTime);
    }
}