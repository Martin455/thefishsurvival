﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace TheFishSurvival.Controls
{
    public class GameUI
    {
        private Button buttonUp;
        private Button buttonDown;
        private const string scoreText = "Score: ";
        private SpriteFont font;
        private Vector2 scorePosition;
        
        public GameUI()
        {
            buttonUp = new Button();
            buttonDown = new Button();
            Score = 0;
        }

        public event EventHandler ButtonUpPressed;
        public event EventHandler ButtonDownPressed;

        public int Score { get; set; }

        public void Init(ContentManager contentManager)
        {
            var textureUp = contentManager.Load<Texture2D>("ArrowUp");
            var textureDown = contentManager.Load<Texture2D>("ArrowDown");
            font = contentManager.Load<SpriteFont>("menufont"); //todo new font

            buttonUp.Init(textureUp);
            buttonUp.Position = new Rectangle(new Point(75, HardwareInformation.ScreenHeight - 125), new Point(100, 100));
            buttonUp.ButtonPressed += ButtonUp_ButtonPressed;

            buttonDown.Init(textureDown);
            buttonDown.Position = new Rectangle(new Point(HardwareInformation.ScreenWidth - 175, HardwareInformation.ScreenHeight - 125), new Point(100, 100));
            buttonDown.ButtonPressed += ButtonDown_ButtonPressed;

            scorePosition = new Vector2(75, 20);
        }

        public void DrawControls(SpriteBatch spriteBatch)
        {
            buttonUp.Draw(spriteBatch, Color.White);
            buttonDown.Draw(spriteBatch, Color.White);

            spriteBatch.DrawString(font, scoreText + Score.ToString(), scorePosition, Color.Black);
        }

        private void ButtonDown_ButtonPressed(object sender, EventArgs e)
        {
            ButtonDownPressed?.Invoke(this, EventArgs.Empty);
        }

        private void ButtonUp_ButtonPressed(object sender, EventArgs e)
        {
            ButtonUpPressed?.Invoke(this, EventArgs.Empty);
        }
    }
}