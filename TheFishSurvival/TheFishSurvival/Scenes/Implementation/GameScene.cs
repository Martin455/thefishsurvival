﻿using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using TheFishSurvival.Scenes.Interfaces;
using TheFishSurvival.Particles;
using TheFishSurvival.Entities;
using TheFishSurvival.Controls;

namespace TheFishSurvival.Scenes.Implementation
{
    public class GameScene : IScene
    {

        #region Fields

        private Texture2D[] backgrounds;
        private Bubbles bubbles;
        private int activeBackground;
        private List<Entity> entities;

        private GameUI ui;
        #endregion

        #region Constructor

        public GameScene(SceneTypes sceneType)
        {
            SceneType = SceneTypes.GameScene;
            bubbles = new Bubbles();
            backgrounds = new Texture2D[3];
            entities = new List<Entity>();
            ui = new GameUI();
        }

        #endregion

        #region Properties

        public SceneTypes SceneType { get; private set; }

        #endregion

        #region Public Methods

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(backgrounds[activeBackground], new Rectangle(new Point(0, 0), new Point(HardwareInformation.ScreenWidth, HardwareInformation.ScreenHeight)), Color.White);
            bubbles.Draw(spriteBatch);

            foreach (var entity in entities)
            {
                entity.Draw(spriteBatch);
            }

            ui.DrawControls(spriteBatch);
        }

        public void Initialization(ContentManager content)
        {
            BackgroundInit(content);
            ui.Init(content);
            bubbles.Init(content);
        }

        public void Update(GameTime gameTime)
        {
            bubbles.Update(gameTime);

            foreach (var entity in entities)
            {
                entity.Update(gameTime);
            }
        }

        #endregion

        #region Private Methods

        private void BackgroundInit(ContentManager content)
        {
            backgrounds[0] = content.Load<Texture2D>("simplebackground");
            activeBackground = 0;
        }

        #endregion 
    }
}