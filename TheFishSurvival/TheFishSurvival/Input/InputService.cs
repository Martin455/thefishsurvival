﻿﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;

namespace TheFishSurvival.Input
{
    public class InputService
    {
        public static InputService Instance { get; }

        static InputService()
        {
            Instance = new InputService();
        }

        private TimeSpan? lastTime = null;
        private readonly TimeSpan timeOffset = TimeSpan.FromMilliseconds(200);

        private InputService()
        {
        }

        public Point InvalidPoint { get; } = new Point(-1);

        public event EventHandler<EventArgs<Point>> DisplayPressed;

        public void DetectInput(GameTime gameTime)
        {
            TimeSpan currentTime = gameTime.TotalGameTime;

            if (lastTime == null || (lastTime.Value + timeOffset) > currentTime)
            {
                TouchCollection touchCollection = TouchPanel.GetState();

                if (touchCollection.Count > 0)
                {
                    if (touchCollection[0].State == TouchLocationState.Pressed || touchCollection[0].State == TouchLocationState.Moved)
                    {
                        DisplayPressed?.Invoke(this, new EventArgs<Point>(new Point((int)touchCollection[0].Position.X, (int)touchCollection[0].Position.Y)));
                    }
                }

                lastTime = currentTime;
            }
        }
    }

    public class EventArgs<T> : EventArgs
    {
        public EventArgs(T data)
        {
            Data = data;
        }

        public T Data { get; }
    }
}